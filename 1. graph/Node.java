import java.util.ArrayList;
import java.util.List;
 
public class Node {

    private final String city;
    private final List<Edge> adjacents = new ArrayList<>();

    public Node(final String city) {
        this.city = city;
    }

    public void addEdge(final Edge edge) {
        adjacents.add(edge);
    }
 
    public List<Edge> getAdjacents() {
        return adjacents;
    }
 
    public String getCity() {
        return city;
    }
 
    @Override
    public String toString() {
        return "Node [city=" + city + ", adjacents=" + adjacents + "]";
    }
}