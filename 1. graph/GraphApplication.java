import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@SuppressWarnings("unchecked")
public class GraphApplication {

    private Graph graph;
 
    public GraphApplication() {
        graph = MapBuilder.getGraph();
    }
 
    private Optional getNode(String city) {
        List<Node> nodes = graph.getNodes();
        for (Node node : nodes) {
            if (node.getCity().equals(city)) {
                return Optional.of(node);
            }
        }
        return Optional.empty();
    }
 
    public boolean hasPathDfs(String source, String destination) {
        Optional start = getNode(source);
        Optional end = getNode(destination);
        if (start.isPresent() && end.isPresent()) {
            return hasPathDfs( (Node) start.get() , (Node) end.get(), new HashSet() );
        } else {
            return false;
        }
    }
 
    private boolean hasPathDfs(Node source, Node destination, HashSet visited) {
        if (visited.contains(source.getCity())) {
            return false;
        }
        
        System.out.print(source.getCity() + " -> ");        
        visited.add(source.getCity());

        if (source == destination) {
            return true;
        }
        for (Edge edge : source.getAdjacents()) {
            if (hasPathDfs(edge.getDestination(), destination, visited)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
 
        // Paths from DF
        System.out.println( "\n\t Paths from DF \n" );
        System.out.println( String.format("From DF to DF %s", new GraphApplication().hasPathDfs("DF", "DF")) );
        System.out.println( String.format("From DF to Toluca %s", new GraphApplication().hasPathDfs("DF", "Toluca")) );
        System.out.println( String.format("From DF to Cuernavaca %s", new GraphApplication().hasPathDfs("DF", "Cuernavaca")) );
        System.out.println( String.format("From DF to Puebla %s", new GraphApplication().hasPathDfs("DF", "Puebla")) );
        System.out.println( String.format("From DF to Tlaxcala %s", new GraphApplication().hasPathDfs("DF", "Tlaxcala")) );
        System.out.println( String.format("From DF to Cancún %s", new GraphApplication().hasPathDfs("DF", "Cancún")) );
     
        
        // Paths from Toluca
        System.out.println( "\n\t Paths from Toluca \n" );
        System.out.println( String.format("From Toluca to Toluca %s", new GraphApplication().hasPathDfs("Toluca", "Toluca")) );
        System.out.println( String.format("From Toluca to DF %s", new GraphApplication().hasPathDfs("Toluca", "DF")) );
        System.out.println( String.format("From Toluca to Cuernavaca %s", new GraphApplication().hasPathDfs("Toluca", "Cuernavaca")) );
        System.out.println( String.format("From Toluca to Puebla %s", new GraphApplication().hasPathDfs("Toluca", "Puebla")) );
        System.out.println( String.format("From Toluca to Tlaxcala %s", new GraphApplication().hasPathDfs("Toluca", "Tlaxcala")) );
        System.out.println( String.format("From Toluca to Cancún %s", new GraphApplication().hasPathDfs("Toluca", "Cancún")) );
    
        // Paths from Cuernavaca
        System.out.println( "\n\t Paths from Cuernavaca \n" );
        System.out.println( String.format("From Cuernavaca to Cuernavaca %s", new GraphApplication().hasPathDfs("Cuernavaca", "Cuernavaca")) );
        System.out.println( String.format("From Cuernavaca to DF %s", new GraphApplication().hasPathDfs("Cuernavaca", "DF")) );
        System.out.println( String.format("From Cuernavaca to Toluca %s", new GraphApplication().hasPathDfs("Cuernavaca", "Toluca")) );
        System.out.println( String.format("From Cuernavaca to Puebla %s", new GraphApplication().hasPathDfs("Cuernavaca", "Puebla")) );
        System.out.println( String.format("From Cuernavaca to Tlaxcala %s", new GraphApplication().hasPathDfs("Cuernavaca", "Tlaxcala")) );
        System.out.println( String.format("From Cuernavaca to Cancún %s", new GraphApplication().hasPathDfs("Cuernavaca", "Cancún")) );
    
        // Paths from Puebla
        System.out.println( "\n\t Paths from Puebla \n" );
        System.out.println( String.format("From Puebla to Puebla %s", new GraphApplication().hasPathDfs("Puebla", "Puebla")) );
        System.out.println( String.format("From Puebla to Cuernavaca %s", new GraphApplication().hasPathDfs("Puebla", "Cuernavaca")) );
        System.out.println( String.format("From Puebla to DF %s", new GraphApplication().hasPathDfs("Puebla", "DF")) );
        System.out.println( String.format("From Puebla to Toluca %s", new GraphApplication().hasPathDfs("Puebla", "Toluca")) );
        System.out.println( String.format("From Puebla to Tlaxcala %s", new GraphApplication().hasPathDfs("Puebla", "Tlaxcala")) );
        System.out.println( String.format("From Puebla to Cancún %s", new GraphApplication().hasPathDfs("Puebla", "Cancún")) );
    
        // Paths from Tlaxcala
        System.out.println( "\n\t Paths from Tlaxcala \n" );
        System.out.println( String.format("From Tlaxcala to Tlaxcala %s", new GraphApplication().hasPathDfs("Tlaxcala", "Tlaxcala")) );
        System.out.println( String.format("From Tlaxcala to Puebla %s", new GraphApplication().hasPathDfs("Tlaxcala", "Puebla")) );
        System.out.println( String.format("From Tlaxcala to Cuernavaca %s", new GraphApplication().hasPathDfs("Tlaxcala", "Cuernavaca")) );
        System.out.println( String.format("From Tlaxcala to DF %s", new GraphApplication().hasPathDfs("Tlaxcala", "DF")) );
        System.out.println( String.format("From Tlaxcala to Toluca %s", new GraphApplication().hasPathDfs("Tlaxcala", "Toluca")) );
        System.out.println( String.format("From Tlaxcala to Cancún %s", new GraphApplication().hasPathDfs("Tlaxcala", "Cancún")) );
    

        // Paths from Cancún
        System.out.println( "\n\t Paths from Cancún \n" );
        System.out.println( String.format("From Cancún to Cancún %s", new GraphApplication().hasPathDfs("Cancún", "Cancún")) );
        System.out.println( String.format("From Cancún to Tlaxcala %s", new GraphApplication().hasPathDfs("Cancún", "Tlaxcala")) );
        System.out.println( String.format("From Cancún to Puebla %s", new GraphApplication().hasPathDfs("Cancún", "Puebla")) );
        System.out.println( String.format("From Cancún to Cuernavaca %s", new GraphApplication().hasPathDfs("Cancún", "Cuernavaca")) );
        System.out.println( String.format("From Cancún to DF %s", new GraphApplication().hasPathDfs("Cancún", "DF")) );
        System.out.println( String.format("From Cancún to Toluca %s", new GraphApplication().hasPathDfs("Cancún", "Toluca")) );
       
    }
}