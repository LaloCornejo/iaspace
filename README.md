---

## Algoritmos para el curso de Inteligencia Artificial

Se trata de una colección de diferentes algoritmo para usar en las clases, prácticas y proyectos de la asignatura de Inteligencia en la Licenciatura en Ciencias Computacionales en el Área Académica de Computación y Electrónica del Instituto de Ciencias Básicas e Ingeniería.

Los proyectos incorporados son:

1. Búsqueda primero en profundidad (DFS) para el recorrido de grafos.
2. Búsqueda primero en profundidad (DFS) para las solución de laberintos.
3. Algositmos de búsqueda para la solución de 8 puzzle

---

## Forma de usar los códigos

El propósito es académico y se consideran las siguientes actividades en cada proyecto para reforzar los temas teóricos.

1. Identifica los componentes del proyecto: **clases, relaciones y propiedades**.
2. **Compila** el proyecto.
3. **Ejecuta** el proyecto.
4. **Analiza** el funcionamiento y resultados.
5. **Diseña** diagrama de clases, mapas conceptuales, diagramas UML u otra artefctos que te ayude a comprender el funcionamiento.

Para sacar más provecho los códigos en las clases se describirán los **retos** y **nuevas funcionalidades** que deberás agregar a cada proyecto.

---

## Para compilar

Sigue los siguientes pasos para compilar los códigos en cada proyecto.

1. Lee el archivo **instrucciones.txt** donde se especifican las **herramientas** y **versiones** que necesitas en cada proyecto.
2. En el mismo archivo, dependiendo del caso, es posible encontrar material audiovisual que ayuda a entender el proyecto.
3. Instala y configurar las herramientas de **edición, compilación y prueba**.
4. Ejecuta los proyectos y analiza el funcionamiento de todos los **componentes** y **resultados**.

En todos los casos, los códigos están listos para ser compilados. No tendrás necesidad de corregir bugs.

---

## Para clonar el repositorio

Sigue los siguientes pasos para clonar el repositorio en tu equipo de cómputo personal.

1. Instalar el **Git** en tu equipo personal para el control de versiones del repositorio.
2. Configura las variables de **email** y **username**.
3. Ejecuta el comando **git clone https://LaloCornejo@bitbucket.org/LaloCornejo/iaspace.git**


## Git también es para ti

Deseas aprender a usar el control de versiones de **Git**, sigue los siguientes pasos:

1. Para que compartas tus códigos crea una carpeta dentro del repositorio con tu nombre.
2. Ejecuta el comando **commit** cuando modifiques los códigos y sean registrados en el control de versiones.
3. Ejecuta el comando **push** para publicar tus modificaciones y actualizaciones.
4. Ejecuta el comando **pull** para descargar las modificaciones y actualizaciones de tus compañeros.

NOTA IMPORTANTE. Después de hacer el **push** de tus códigos, estos pasan a ser de dominio y ciencia de los memes públicos.